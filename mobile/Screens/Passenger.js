import React, {Component} from 'react';
import {
    Platform,
    SafeAreaView,
    Alert,
    StyleSheet,
    Text,
    View,
    TextInput,
    Button,
    TouchableOpacity,
    TouchableHighlight,
    ActivityIndicator,
    PermissionsAndroid,
    Image, Dimensions,
} from 'react-native';
import SocketIO from "socket.io-client"
import MapView, {Polyline, Marker} from "react-native-maps";
import _ from "lodash";
import PolyLine from "@mapbox/polyline";

var {height, width} = Dimensions.get('window')
export default class Passenger extends Component {
    static navigationOptions = {
        title: 'Passenger', header: null
    };

    state = {
        error: "",
        latitude: 0,
        longitude: 0,
        destinationInput: '',
        pickupInput: '',
        predictions:[],
        pointCoords: [],
        placeInput: 'Where to?',

        destLatitude: -999,
        destLongitude: -999,
        originLatitude: -999,
        originLongitude: -999,
        distance: -999,

        isLoading: false
    };

    // constructor(props){
    //     super(props);
    //     this.onChangeDestinationDebounced = _.debounce(
    //         this.onChangeDestination,
    //         500
    //     )
    // }

    didSelectLocation = (marker, name, points) => {
        // Lat, lng => from Google Geolocation API
        if (marker === 'destination') {
            this.setState({latitude: points.lat, longitude: points.lng, destLatitude: points.lat, destLongitude: points.lng, destinationInput: name});
        } else {
            this.setState({latitude: points.lat, longitude: points.lng, originLatitude: points.lat, originLongitude: points.lng, pickupInput: name});
        }

        this.getRouteDirections()
    };

    componentDidMount() {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                this.setState( {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude
                })
            },
            (error) => {
                Alert.alert('Gagal Memuat', 'Periksa koneksi anda')
            }, {enableHighAccuracy: true, maximumAge: 2000, timeout: 20000}
        )
    }

    async getRouteDirections() {
        if (this.state.destinationInput !== '' && this.state.pickupInput !== '') {
            const apikey = "AIzaSyCvNbQ9GEZ3p3-U2yamS8UrfHmD_e7Iqmw";
            const apiUrl = `https://maps.googleapis.com/maps/api/directions/json?origin=${this.state.originLatitude}, ${this.state.originLongitude}
                    &destination=${this.state.destLatitude}, ${this.state.destLongitude}&key=${apikey}`;

            try {
                const result = await fetch(apiUrl);
                const json = await result.json();
                const points = PolyLine.decode(json.routes[0].overview_polyline.points);
                const pointCoords = points.map((point) => {
                    return {latitude: point[0], longitude: point[1]}
                });
                this.setState({pointCoords: pointCoords, distance: this.getDistanceOriginDestination()});
                console.log('Distance:', this.state.distance);

                Keyboard.dismiss();

                // Don't log this! Too big object
                // console.log('Map', this.map);

                this.map.fitToCoordinates(pointCoords, {
                    edgePadding: {top: 160, bottom: 120, left: 20, right: 20}
                })

            } catch (error) {
                console.log(error);
            }
        }
    }

    rad = (x) => {
        return x * Math.PI / 180;
    };

    getDistanceOriginDestination = () => {
        let R = 6378137; // Earth’s mean radius in meter
        let dLat = this.rad(Math.abs(this.state.destLatitude - this.state.originLatitude));
        let dLong = this.rad(Math.abs(this.state.destLongitude - this.state.originLongitude));
        let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(this.rad(this.state.originLatitude)) * Math.cos(this.rad(this.state.destLatitude)) *
            Math.sin(dLong / 2) * Math.sin(dLong / 2);
        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        console.log('c', c);
        let d = R * c / 1000;

        return Math.round(d * 10) / 10; // returns the distance in km (1 decimal point)
    };

    render() {
        let orderButton = null;
        let originMarker = null;
        let destinationMarker = null;
        let prediction = null;

        if (this.state.pickupInput !== '') {
            originMarker = (
                <Marker coordinate={{latitude: this.state.originLatitude, longitude: this.state.originLongitude}}>
                </Marker>
            )
        }

        if (this.state.destinationInput !== '') {
            destinationMarker = (
                <Marker coordinate={{latitude: this.state.destLatitude, longitude: this.state.destLongitude}}>
                </Marker>
            )
        }

        if (this.state.distance > -999) {

            const touchableContent = this.state.isLoading ? <ActivityIndicator animating={true}/> : <Text style={styles.requestButtonText}>PESAN SEKARANG</Text>

            orderButton = (
                <View style={{marginTop: 'auto', width: width}}>
                    <TouchableOpacity style={styles.requestButton}
                                      disabled={this.state.isLoading}
                                      onPress={() => {
                                          Alert.alert(`Pesanan diterima!`, `Perjalanan dari ${this.state.pickupInput}, ke ${this.state.destinationInput}`, [
                                              {
                                                  text: 'OK',
                                                  style: 'default',
                                                  onPress: () => this.setState({isLoading: true})
                                              }
                                          ]);
                                      }}>
                        {touchableContent}
                    </TouchableOpacity>
                </View>
            )
        }
        // const predictions = this.state.predictions.map(prediction => (
        //     <TouchableHighlight
        //         key={prediction.id}
        //         onPress={() => this.getRouteDirections(prediction.place_id, prediction.description)}
        //     >
        //         <View>
        //             <Text style={styles.suggestions} key={prediction.id}>{prediction.description}</Text>
        //         </View>
        //     </TouchableHighlight>
        // ));

        return (
            <SafeAreaView style={styles.container}>
                <MapView
                    ref={ map =>
                        this.map = map
                    }
                    style={styles.map}
                    onMapReady={ () => {
                        if (Platform.OS === 'android') {
                            PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
                                .then( (isGranted) => {
                                    if (isGranted) {
                                        console.log('Permission granted')
                                    } else {
                                        Alert.alert('Permission denied', 'Izinkan lokasi saat ini')
                                    }
                                })
                        }
                    }}
                    region={{
                        latitude: this.state.latitude,
                        longitude: this.state.longitude,
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421,
                    }}
                    showsUserLocation={true}
                >
                    {originMarker}
                    {destinationMarker}
                    <Polyline
                        coordinates={this.state.pointCoords}
                        strokeWidth={2.5}
                        strokeColor={"red"}
                    />

                </MapView>
                <View

                >
                    <Image
                        source={require('../assets/icons/menu.png')}
                        style={styles.menuIcon}
                    />

                    <TextInput
                        multiline={true}
                        placeholder={"Where to?"}
                        style={styles.destination}
                        value={this.state.destinationInput}
                        onFocus={ () => this.state.isLoading ? Keyboard.dismiss() : this.props.navigation.push('ListPlaces', {didSelectLocation: (name, point) => this.didSelectLocation('destination', name, point) })}

                    />

                    <TextInput
                        multiline={true}
                        placeholder={"Where are you?"}
                        style={styles.destination}
                        value={this.state.pickupInput}
                        onFocus={ () => this.state.isLoading ? Keyboard.dismiss() : this.props.navigation.push('ListPlaces', {didSelectLocation: (name, point) => this.didSelectLocation('origin', name, point) })}

                    />
                </View>
                {orderButton}

            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    bottomButton: {
        backgroundColor: "#010a87",
        marginTop: "auto",
        margin: 20,
        padding: 15,
        paddingHorizontal: 30,
        alignSelf: "center",
        borderRadius: 10,
    },
    bottomButtonText: {
        color: "#fff",
        fontSize: 20,
    },
    suggestions: {
        backgroundColor: '#fff',
        padding: 5,
        fontSize: 18,
        borderWidth: 0.5,
        marginHorizontal: 5,
    },

    destination: {
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: 10,
        paddingVertical: 20,
        height: 40,
        marginTop: 50,
        marginHorizontal: 10,
        backgroundColor: '#fff',
        elevation: 5,
        shadowOpacity: 0.3,
        shadowRadius: 3,
        shadowOffset: {
            height: 0,
            width: 0
        },
    },
    destinationText: {
        color: '#1b2280',
        marginLeft: 5,
    },
    menuIcon: {
        height: 20,
        width: 20,
        marginTop: 50,
        marginHorizontal: 10
    },
    squareDot: {
        marginHorizontal: 10,
        height: 5,
        width: 5,
        backgroundColor: 'black'
    },
    container: {
        ...StyleSheet.absoluteFillObject,
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
});
