import React, {Component} from 'react';
import {
    Alert,
    Platform,
    StyleSheet,
    Text,
    View,
    TextInput,
    Dimensions,
    TouchableOpacity,
    TouchableHighlight,
    ActivityIndicator,
    SafeAreaView,
    Image,
    PermissionsAndroid
} from 'react-native';
import SocketIO from "socket.io-client"
import MapView, {PROVIDER_GOOGLE, PROVIDER_DEFAULT, Polyline, Marker} from "react-native-maps";
import _ from "lodash";
import PolyLine from "@mapbox/polyline";

var {height, width} = Dimensions.get('window')
export default class Passenger extends Component {
    static navigationOptions = {
        title: 'Passenger', header: null
    };

    constructor(props){
        super(props);
        this.state = {
            error: "",
            latitude: 0,
            longitude: 0,
            pickup: "",
            destination: "",
            predictions:[],
            pointCoords: [],
            routeResponse: null,
            driverButtonPressed: false,

            destLatitude: -999,
            destLongitude: -999,
            originLatitude: -999,
            originLongitude: -999,
            distance: -999,

        };
        this.onChangeDestinationDebounced = _.debounce(
            this.onChangeDestination,
            500
        )

        this.onChangePickupDebounced = _.debounce(
            this.onChangePickup,
            500
        )
    }

    didChooseLocation = async (name, placeId) => {
        console.log('Point:', placeId);
        const apikey = "AIzaSyCvNbQ9GEZ3p3-U2yamS8UrfHmD_e7Iqmw";
        const apiUrl = `https://maps.googleapis.com/maps/api/geocode/json?place_id=${placeId}&key=${apikey}`;
        try {
            const result = await fetch(apiUrl);
            const json = await result.json();

            let point = json.results[0].geometry.location;
            console.log('location point', point);
            // this.props.navigation.state.params.didSelectLocation(name, point);
            // this.props.navigation.pop();
        } catch (error) {
            console.log('DidSelectLocation', error);
        }
        // this.didGetLocation()
        this.getRouteDirections()
    };

    didGetLocation = (marker, name, points) => {
        if (marker === 'destination') {
            this.setState({latitude: points.lat, longitude: points.lng, destLatitude: points.lat, destLongitude: points.lng, destinationInput: name});
        } else {
            this.setState({latitude: points.lat, longitude: points.lng, originLatitude: points.lat, originLongitude: points.lng, pickupInput: name});
        }

        this.getRouteDirections()
    };

    didSelectLocation = (destination, placeId, place_id) => {
        let title = 'Location selected';

        if (destination === '') {
            destination = 'Where to?';
            title = 'You did not select any location'
        }

        this.props.navigation.state.params.didSelectLocation(destination, placeId, place_id);

        Alert.alert(title, 'Are you sure?', [
            {
                text: 'No',
                style: 'cancel'
            },
            {
                text: 'Yes',
                style: 'default',
                onPress: () => this.props.navigation.pop()

            }
        ])
    }

    async getRouteDirections(placeId, place_id){
        try{
            const apikey = "AIzaSyCvNbQ9GEZ3p3-U2yamS8UrfHmD_e7Iqmw";
            // const response = await fetch(`https://maps.googleapis.com/maps/api/directions/json?origin=${this.state.latitude},${this.state.longitude}&destination=place_id:${placeId}&key=AIzaSyCvNbQ9GEZ3p3-U2yamS8UrfHmD_e7Iqmw`)
            const apiUrl = `https://maps.googleapis.com/maps/api/directions/json?origin=${this.state.originLatitude}, ${this.state.originLongitude}
                    &destination=${this.state.destLatitude}, ${this.state.destLongitude}&key=${apikey}`;
            const result = await fetch(apiUrl);
            const json = await result.json();
            const points = PolyLine.decode(json.routes[0].overview_polyline.points);
            const pointCoords = points.map(point => {
                return{latitude:point[0],longitude:point[1]}}
            );
            this.setState({pointCoords: pointCoords, predictions:[], destination: place_id, routeResponse: place_id, distance: this.getDistanceOriginDestination()});

            Keyboard.dismiss();

            this.map.fitToCoordinates(pointCoords, {
                edgePadding:{top:30, bottom: 100, left:30, right:30}
            });
        } catch(error){
            console.error(error)
        }
    }

    rad = (x) => {
        return x * Math.PI / 100;
    };

    getDistanceOriginDestination = () => {
        let R = 6378137; // Earth’s mean radius in meter
        let dLat = this.rad(Math.abs(this.state.destLatitude - this.state.originLatitude));
        let dLong = this.rad(Math.abs(this.state.destLongitude - this.state.originLongitude));
        let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(this.rad(this.state.originLatitude)) * Math.cos(this.rad(this.state.destLatitude)) *
            Math.sin(dLong / 2) * Math.sin(dLong / 2);
        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        console.log('c', c);
        let d = R * c / 1000;

        return Math.round(d * 10) / 10; // returns the distance in km (1 decimal point)
    };

    async onChangePickup(pickup){
        const apikey = "AIzaSyCvNbQ9GEZ3p3-U2yamS8UrfHmD_e7Iqmw";
        const apiUrl = `https://maps.googleapis.com/maps/api/place/autocomplete/json?key=${apikey}
                        &input=${pickup}&location=${this.state.latitude}, ${this.state.longitude}&radius=2000`;
        try{
            const result = await fetch(apiUrl);
            const json = await result.json();
            console.log("iniJson");
            console.log(json);
            this.setState({predictions: json.predictions})
        } catch (e) {
            console.log(e)
        }
    }

    async onChangeDestination(destination){
        const apikey = "AIzaSyCvNbQ9GEZ3p3-U2yamS8UrfHmD_e7Iqmw";
        const apiUrl = `https://maps.googleapis.com/maps/api/place/autocomplete/json?key=${apikey}
                        &input=${destination}&location=${this.state.latitude}, ${this.state.longitude}&radius=2000`;
        try{
            const result = await fetch(apiUrl);
            const json = await result.json();
            console.log("iniJson");
            console.log(json);
            this.setState({predictions: json.predictions})
        } catch (e) {
            console.log(e)
        }
    }

    componentDidMount() {
        navigator.geolocation.getCurrentPosition(
            position => {
                this.setState( {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude
                })
                // this.getRouteDirections();
            },
            error => this.setState({error: error.message}),
            {enableHighAccuracy: true, maximumAge: 2000, timeout: 20000}
        )

    }

    render() {
        let marker = null;
        let requestButton = null;

        if(this.state.pointCoords.length >1) {
            marker = (
                <Marker coordinate={this.state.pointCoords[this.state.pointCoords.length - 1]}/>
            );
            requestButton = (
                <View style={{flex: 1, justifyContent: 'flex-end'}}
                >
                    <TouchableOpacity style={styles.requestButton}
                    >
                        <Text style={styles.requestButtonText}
                        >
                            REQUEST
                        </Text>
                    </TouchableOpacity>
                </View>
            )
        };

        let backButton = null;


        let upperSection = (
            <View style={styles.upperSection}
            >
                <View>
                    <TouchableOpacity
                        onPress={ () =>
                            this.didSelectLocation(this.state.destination)
                        }
                    >
                        <Image
                            source={require('../assets/icons/back-left-arrow.png')}
                            style={styles.backIcon}
                        />
                    </TouchableOpacity>

                </View>
                <View
                    style={styles.pickupView}
                >
                    <View
                        style={styles.squareDotGrey}
                    />
                    <TextInput
                        multiline={true}
                        placeholder={"Current Location"}
                        value={this.state.pickup}
                        style={styles.pickup}
                        onChangeText={pickup => {
                            this.setState()
                        }}
                        // onChangeText={pickup => {
                        //     this.setState({pickup});
                        //     this.onChangePickupDebounced(pickup)
                        // }}
                    />

                </View>
                <View
                    style={styles.destinationView}
                >
                    <View
                        style={styles.squareDot}
                    />
                    <TextInput
                        placeholder={"Where to?"}
                        value={this.state.destination}
                        style={styles.destination}
                        onChangeText={destination => {
                            this.setState({destination});
                            this.onChangeDestinationDebounced(destination)
                        }}
                    />
                    <Image
                        source={require('../assets/icons/plus-sign.png')}
                        style={styles.plusIcon}
                    />
                </View>
            </View>
        );

        if(this.state.pointCoords.length >1){
            upperSection = null;
            backButton = (
                <View>
                    <TouchableOpacity
                        onPress={ () =>
                            this.setState({pointCoords: [],
                                // destination: '',
                                // pickup: ''
                            })
                        }
                    >
                        <Image
                            source={require('../assets/icons/back-left-arrow.png')}
                            style={styles.backIcon}
                        />
                    </TouchableOpacity>
                </View>
            );
            marker = (
                <Marker coordinate={this.state.pointCoords[this.state.pointCoords.length-1]}/>
            );

            requestButton = (
                <View style={{flex: 1}}
                >
                    <View style={{flex: 2, justifyContent: 'flex-end', alignItems: 'center'}}
                    >

                    </View>

                    <View style={{flex: 1, backgroundColor: '#fff', alignItems: 'center', }}
                    >
                        <Image source={require('../assets/icons/globe.png')}
                               style={{margin: 30, height: 70, width: 70}}
                        />
                    </View>

                    <View style={{flex: 1, justifyContent: 'flex-end', backgroundColor: '#fff'}}
                    >
                        <TouchableOpacity style={styles.paymentDropDown}
                        >
                            <Image source={require('../assets/icons/dollar-bill.png')}
                                   style={styles.paymentImage}
                            />
                            <Text style={styles.paymentText}
                            >
                                Cash
                            </Text>
                            <Image source={require('../assets/icons/drop-down-arrow.png')}
                                   style={{height: 5, width: 5, marginHorizontal: 5}}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.requestButton}
                        >
                            <Text style={styles.requestButtonText}
                            >
                                REQUEST
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>


            )
        }

        const predictions = this.state.predictions.map(prediction => (
            <TouchableHighlight

                key={prediction.id}
                onPress={() => this.didChooseLocation(prediction.place_id, prediction.description)}
                // underlayColor={'#fff'}
            >
                <View style={styles.predictions}
                >
                    <View
                        style={styles.locationCircle}
                    >
                        <Image
                            source={require('../assets/icons/locationplaceholder.png')}
                            style={styles.locationIcon}
                        />
                    </View>

                    <View>
                        <Text style={styles.suggestions} key={prediction.id}>{prediction.description}</Text>
                    </View>
                </View>
            </TouchableHighlight>
        ));

        return (
            <SafeAreaView style={styles.container}>
                <MapView
                    ref={ map =>
                        this.map = map
                    }

                    style={styles.map}
                    region={{
                        latitude: this.state.latitude,
                        longitude: this.state.longitude,
                        latitudeDelta: 0.015,
                        longitudeDelta: 0.0121,
                    }}
                    showsUserLocation={true}
                >
                    <Polyline
                        coordinates={this.state.pointCoords}
                        strokeWidth={2.5}
                        strokeColor={"red"}
                    />
                    {marker}
                </MapView>
                {backButton}



                {upperSection}
                {predictions}
                {requestButton}

            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    upperSection: {
        backgroundColor: '#fff',
        paddingBottom: 10,
        elevation: 10,
        shadowOpacity: 0.3,
        shadowRadius: 3,
        shadowOffset: {
            height: 0,
            width: 0
        },

    },
    bottomButtonText: {
        color: "#fff",
        fontSize: 20,
    },
    suggestions: {
        padding: 5,
        fontSize: 18,
        borderBottomColor: '#808080',
        marginHorizontal: 5,
    },
    predictions: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 20,
        backgroundColor: '#fff'
    },
    pickupView: {
        flexDirection: 'row',
        alignItems: 'center',
        width: width,
        marginBottom: 10,
    },
    pickup: {
        alignItems: 'center',
        flexDirection: 'row',
        padding: 10,
        height: 40,
        width: '70%',
        marginHorizontal: 10,
        backgroundColor: '#fafafa'
    },
    destinationView: {
        flexDirection: 'row',
        alignItems: 'center',
        width: width,
    },
    destination: {
        alignItems: 'center',
        flexDirection: 'row',
        padding: 10,
        height: 40,
        width: '70%',
        marginHorizontal: 10,
        backgroundColor: '#ededed'
    },
    destinationText: {
        color: '#808080',
        marginLeft: 10,
    },
    backIcon: {
        height: 20,
        width: 20,
        marginTop: 50,
        marginHorizontal: 10
    },
    plusIcon: {
        height: 15,
        width: 15,
        marginLeft: 15
    },
    locationIcon: {
        height: 15,
        width: 15,
    },
    locationCircle: {
        height: 30,
        width: 30,
        borderRadius: width/2,
        backgroundColor: '#7a7a7a',
        alignItems: 'center',
        justifyContent: 'center'
    },
    squareDotGrey: {
        marginHorizontal: 20,
        height: 5,
        width: 5,
        backgroundColor: '#808080'
    },
    squareDot: {
        marginHorizontal: 20,
        height: 5,
        width: 5,
        backgroundColor: 'black'
    },
    paymentDropDown: {
        flexDirection: 'row',
        borderTopColor: '#f2f2f2',
        borderTopWidth: 1,
        alignItems: 'center',
        backgroundColor: '#fff'
    },
    paymentImage: {
        height: 50,
        width: 50,
        marginHorizontal: 5,
    },
    paymentText: {
        color: '#000',
        marginHorizontal: 5,
        fontWeight: 'bold'
    },
    requestButton: {
        backgroundColor: "#bdbdbd",
        alignItems: 'center'
    },
    requestButtonText: {
        color: '#fff',
        paddingVertical: 15,
        fontWeight: 'bold'
    },
    container: {
        ...StyleSheet.absoluteFillObject,
    },
    map: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        // height: '100%'
    },
});
