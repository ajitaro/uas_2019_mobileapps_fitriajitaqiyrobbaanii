import React, {Component} from 'react';
import {
    createStackNavigator,
    createAppContainer,
    createSwitchNavigator
} from 'react-navigation';
import ListPlaces from "./Screens/ListPlaces"
import Passenger from "./Screens/Passenger";

const AppStack = createStackNavigator(
    {
    Passenger: {screen: Passenger},
    ListPlaces: {screen: ListPlaces},
    });

export default createAppContainer(createSwitchNavigator(
    {
        App: AppStack,
    },
    {
        initialRouteName: 'App'
    }
))